import React from 'react';
import PropTypes from 'prop-types';

import Devtools from './Devtools';
import Main from '../pages/main';


const Frame = (props) => {
    const { children } = props;
    return (
        <div>
            <Main>
                {children}
            </Main>
            <Devtools />
        </div>
    );
};

Frame.propTypes = {
    children: PropTypes.node,
};

Frame.defaultProps = {
    children: null,
};

export default Frame;
