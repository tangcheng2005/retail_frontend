/* eslint-disable no-console */
import React from 'react';

import { Router, Route, hashHistory, IndexRoute } from 'react-router';

import Frame from './Frame';
import Home from '../pages/home';
import StockPage from '../pages/stock';
import StatistcsPage from '../pages/statistics';
import Category from '../pages/main/components/category';
import ProductDetail from '../pages/main/components/productDetail';

const handleChange = (...args) => {
    console.log(args);
};

const Routers = () => (
    <Router history={hashHistory}>
        <Route path="/" component={Frame} onChange={handleChange}>
            <IndexRoute component={Home} />
            <Route path="category" component={Category} />
            <Route path="stock" component={StockPage} />
            <Route path="statistics" component={StatistcsPage} />
            <Route path="productDetail/:sku" component={ProductDetail} />
        </Route>
    </Router>
);

export default Routers;
