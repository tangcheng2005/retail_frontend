import { handleActions } from 'redux-actions';
import immutable from 'immutable';

const initState = immutable.fromJS([]);

export default handleActions({
    getStock_SUCCESS: (state, action) => {
        const stock = action.payload.data;
        return state.concat(stock);
    },
}, initState);
