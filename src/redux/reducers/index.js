import { combineReducers } from 'redux';
import test from './testReducers';
import category from './categoryReducers';
import modal from './modalReducers';
import product from './productReducers';
import stock from './stockReducer';

export default combineReducers({
    test,
    category,
    modal,
    product,
    stock,
});
