import { createApiAction } from '../../reduxHelper/actionHelper';

export const getStock = createApiAction('getStock', {
    url: '/stock',
    method: 'get',
});
