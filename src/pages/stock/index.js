import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Table } from 'antd';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getStock } from '../../redux/actions/stockActions';

const mapStateToProps = state => ({
    stock: state.stock,
});

const mapDispatchToProps = {
    getStock,
};

const getColumns = () =>
    [
        {
            title: 'SKU',
            dataIndex: 'sku',
            key: 'sku',
        },
        {
            title: '商品名',
            dataIndex: 'productName',
            key: 'productName',
        },
        {
            title: '商品编码',
            dataIndex: 'productCode',
            key: 'productCode',
        },
        {
            title: '库存',
            dataIndex: 'inventory',
            key: 'inventory',
        },
        {
            title: '已售',
            dataIndex: 'sold',
            key: 'sold',
        },
        {
            title: '售格',
            dataIndex: 'price',
            key: 'price',
        },
    ];

@connect(mapStateToProps, mapDispatchToProps)
class StockPage extends Component {
    static propTypes = {
        getStock: PropTypes.func.isRequired,
        stock: PropTypes.instanceOf(List).isRequired,
    }
    componentDidMount() {
        const { getStock } = this.props;
        getStock();
    }
    render() {
        const { stock } = this.props;
        return (
            <div>
                <Table dataSource={stock.toJS()} columns={getColumns()} />
            </div>
        );
    }
}

export default StockPage;
