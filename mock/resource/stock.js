module.exports = {
    result: 1,
    data: [{
        key: '1',
        productName: '华为自行车',
        sku: '0000001',
        productCode: '001',
        inventory: 7,
        sold: 100,
        price: 1000

    }, {
        key: '2',
        productName: '华为摩托车',
        sku: '0000002',
        productCode: '002',
        inventory: 7,
        sold: 100,
        price: 1000
    }, {
        key: '3',
        productName: '华为小轿车',
        sku: '0000003',
        productCode: '003',
        inventory: 7,
        sold: 100,
        price: 1000
    }]
};
