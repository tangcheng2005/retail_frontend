module.exports = {
    result: 1,
    data: [{
        key: '1',
        productName: '华为自行车',
        SKU: '0000001',
        productCode: 'code_a',
    }, {
        key: '2',
        productName: '华为摩托车',
        SKU: '0000002',
        productCode: 'code_b',
    }, {
        key: '3',
        productName: '华为小轿车',
        SKU: '0000003',
        productCode: 'code_c',
    }]
};
